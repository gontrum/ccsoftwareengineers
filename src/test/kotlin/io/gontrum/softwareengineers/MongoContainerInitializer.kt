package io.gontrum.softwareengineers

import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.utility.DockerImageName

class MongoContainerInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        val mongo = MongoDBContainer(DockerImageName.parse("mongo:4.2"))
        mongo.start()
        System.setProperty("spring.data.mongodb.uri", mongo.getReplicaSetUrl())
    }
}
