package io.gontrum.softwareengineers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.ninjasquad.springmockk.MockkBean
import io.gontrum.softwareengineers.adapter.GithubClient
import io.gontrum.softwareengineers.controller.SynchronizationController
import io.gontrum.softwareengineers.service.SynchronizationService
import io.mockk.clearMocks
import io.mockk.every
import org.assertj.core.api.Assertions.assertThat
import org.bson.Document
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.findAll
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

@ExtendWith(SpringExtension::class)
@EnableMongoRepositories(basePackageClasses = [SoftwareengineersApplication::class])
@ContextConfiguration(
    initializers = [MongoContainerInitializer::class],
    classes = [SynchronizationController::class, SynchronizationService::class, GithubClient::class, MongoConfiguration::class],
)
@AutoConfigureDataMongo
@WebMvcTest(SynchronizationController::class)
@TestPropertySource(
    properties = ["github.token=", "github.member.url=https://api.github.com/orgs/testorg/members"]
)
class SynchronizationUseCaseTest(
    @Autowired val mongo: MongoTemplate,
    @Autowired val webClient: MockMvc
) {

    @MockkBean(name = "restTemplateGithub")
    private lateinit var restTemplate: RestTemplate

    @AfterEach
    fun cleanup() {
        mongo.dropCollection("codecentricDevs")
        mongo.dropCollection("syncTimes")
        clearMocks(restTemplate)
    }

    @Test
    fun `should initialize with data from github with single codecentricDev`() {
        givenTheMembersEndpoint("members.json")
        givenTheRepositoriesEndpoint("repositories.json", "atestuser")
        givenTheUserEndpoint("user.json", "atestuser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(aDeveloper(id = "atestuser"))
    }

    @Test
    fun `should initialize with data from github with single codecentricDev and multiple githubRepos`() {
        givenTheMembersEndpoint("members.json")
        givenTheRepositoriesEndpoint("multiple_repos.json", "atestuser")
        givenTheUserEndpoint("user.json", "atestuser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(
            aDeveloper(
                id = "atestuser",
                languages = listOf(
                    theLanguage("java", 1),
                    theLanguage("ruby", 1)
                ),
                githubRepos = listOf(
                    theGithubRepo("my-great-project", "https://github.com/atestuser/my-great-project", "A Description"),
                    theGithubRepo(
                        "my-great-project2",
                        "https://github.com/atestuser/my-great-project2",
                        "A Description"
                    )
                )
            )
        )
    }

    @Test
    fun `should initialize with data from github with multiple codecentricDevs and multiple githubRepos`() {
        givenTheMembersEndpoint("multiple_members.json")
        givenTheUserEndpoint("user.json", "atestuser")
        givenTheRepositoriesEndpoint("multiple_repos.json", "atestuser")
        givenTheUserEndpoint("member_b/user.json", "aseconduser")
        givenTheRepositoriesEndpoint("member_b/multiple_repos.json", "aseconduser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(
            aDeveloper(
                id = "atestuser",
                languages = listOf(
                    theLanguage("java", 1),
                    theLanguage("ruby", 1)
                ),
                githubRepos = listOf(
                    theGithubRepo("my-great-project", "https://github.com/atestuser/my-great-project", "A Description"),
                    theGithubRepo(
                        "my-great-project2",
                        "https://github.com/atestuser/my-great-project2",
                        "A Description"
                    )
                )
            ),
            aDeveloper(
                id = "aseconduser",
                repositoryUrl = "https://api.github.com/users/aseconduser/repos",
                userUrl = "https://api.github.com/users/aseconduser",
                name = "Louis Armstrong",
                languages = listOf(
                    theLanguage("javascript", 1),
                    theLanguage("java", 1)
                ),
                githubRepos = listOf(
                    theGithubRepo(
                        "a-javascript-project",
                        "https://github.com/aseconduser/a-javascript-project",
                        "A Description"
                    ),
                    theGithubRepo(
                        "a-java-project",
                        "https://github.com/aseconduser/a-java-project",
                        "A Description"
                    )
                )
            )
        )
    }

    @Test
    fun `should update existing codecentricDev with new data`() {
        givenTheCodecentricDevs(
            aDeveloper(
                name = "Wynton Marsalis",
                languages = listOf(
                    theLanguage("trumpet", 12)
                ),
                githubRepos = emptyList()
            )
        )

        givenTheMembersEndpoint("members.json")
        givenTheRepositoriesEndpoint("repositories.json", "atestuser")
        givenTheUserEndpoint("user.json", "atestuser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(
            aDeveloper(
                name = "Miles Davis",
                languages = listOf(
                    theLanguage("java", 1)
                ),
                githubRepos = listOf(
                    theGithubRepo(
                        "my-great-project",
                        "https://github.com/atestuser/my-great-project",
                        "A Description"
                    )
                )
            )
        )
    }

    @Test
    fun `should keep name of existing codecentricDev when no new user details are provided`() {
        givenTheCodecentricDevs(
            aDeveloper(
                name = "Wynton Marsalis",
                languages = listOf(theLanguage("trumpet", 12)),
                githubRepos = emptyList()
            )
        )

        givenTheMembersEndpoint("members.json")
        givenTheRepositoriesEndpoint("repositories.json", "atestuser")
        givenTheUserEndpointSendsNotModified("atestuser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(
            aDeveloper(
                name = "Wynton Marsalis",
                languages = listOf(theLanguage("java", 1)),
                githubRepos = listOf(
                    theGithubRepo(
                        "my-great-project",
                        "https://github.com/atestuser/my-great-project",
                        "A Description"
                    )
                )
            )
        )
    }

    @Test
    fun `should keep languages of existing codecentricDev when no new repository details are provided`() {
        givenTheCodecentricDevs(
            aDeveloper(
                name = "Wynton Marsalis",
                languages = listOf(theLanguage("trumpet", 12)),
                githubRepos = emptyList()
            )
        )

        givenTheMembersEndpoint("members.json")
        givenTheRepositoriesEndpointSendsNotModified("atestuser")
        givenTheUserEndpoint("user.json", "atestuser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(
            aDeveloper(
                name = "Miles Davis",
                languages = listOf(theLanguage("trumpet", 12)),
                githubRepos = emptyList()
            )
        )
    }

    @Test
    fun `should use data from already synchronized codecentricDevs to update githubRepos and userDetails when members havent changed since last sync`() {
        givenTheCodecentricDevs(
            aDeveloper(
                name = "Wynton Marsalis",
                languages = emptyList(),
                githubRepos = emptyList()
            )
        )

        givenTheMembersEndpointSendsNotModified()
        givenTheRepositoriesEndpoint("repositories.json", "atestuser")
        givenTheUserEndpoint("user.json", "atestuser")

        whenIStartSynchronization()

        thenIExpectANoContentResponse()
        thenIExpectTheCodecentricDeveloperDocuments(
            aDeveloper(
                name = "Miles Davis",
                languages = listOf(theLanguage("java", 1)),
                githubRepos = listOf(
                    theGithubRepo(
                        "my-great-project",
                        "https://github.com/atestuser/my-great-project",
                        "A Description"
                    )
                )
            )
        )
    }

    private lateinit var actions: ResultActions

    private fun whenIStartSynchronization() {
        actions = webClient.perform(put("/synchronize"))
    }

    private fun givenTheCodecentricDevs(codecentricDev: Document) {
        mongo.save(codecentricDev, "codecentricDevs")
    }

    private fun givenTheMembersEndpoint(file: String) {
        val body = SynchronizationUseCaseTest::class.java.getResource("/${file}").readText()
        val headers = HttpHeaders().apply { put("date", listOf("Thu, 05 Jul 2012 15:31:30 GMT")) }
        val response: List<GithubClient.MemberHttpResponse> = jacksonObjectMapper().readValue(body)
        every {
            restTemplate.exchange<List<GithubClient.MemberHttpResponse>>(
                "https://api.github.com/orgs/testorg/members",
                HttpMethod.GET,
                any()
            )
        } returns ResponseEntity(response, headers, HttpStatus.OK)
    }

    private fun givenTheMembersEndpointSendsNotModified() {
        val headers = HttpHeaders().apply { put("date", listOf("Thu, 05 Jul 2012 15:31:30 GMT")) }
        every {
            restTemplate.exchange<List<GithubClient.MemberHttpResponse>>(
                "https://api.github.com/orgs/testorg/members",
                HttpMethod.GET,
                any()
            )
        } returns ResponseEntity(null, headers, HttpStatus.NOT_MODIFIED)
    }

    private fun givenTheRepositoriesEndpoint(file: String, username: String) {
        val body = SynchronizationUseCaseTest::class.java.getResource("/${file}").readText()
        val headers = HttpHeaders().apply { put("date", listOf("Thu, 05 Jul 2012 15:31:30 GMT")) }
        val response: List<GithubClient.RepositoryHttpResponse> = jacksonObjectMapper().readValue(body)
        every {
            restTemplate.exchange<List<GithubClient.RepositoryHttpResponse>>(
                "https://api.github.com/users/${username}/repos",
                HttpMethod.GET,
                any()
            )
        } returns ResponseEntity(response, headers, HttpStatus.OK)
    }

    private fun givenTheRepositoriesEndpointSendsNotModified(username: String) {
        val headers = HttpHeaders().apply { put("date", listOf("Thu, 05 Jul 2012 15:31:30 GMT")) }
        every {
            restTemplate.exchange<List<GithubClient.RepositoryHttpResponse>>(
                "https://api.github.com/users/${username}/repos",
                HttpMethod.GET,
                any()
            )
        } returns ResponseEntity(null, headers, HttpStatus.NOT_MODIFIED)
    }

    private fun givenTheUserEndpointSendsNotModified(username: String) {
        val headers = HttpHeaders().apply { put("date", listOf("Thu, 05 Jul 2012 15:31:30 GMT")) }
        every {
            restTemplate.exchange<GithubClient.UserDetailsHttpResponse>(
                "https://api.github.com/users/${username}",
                HttpMethod.GET,
                any()
            )
        } returns ResponseEntity(null, headers, HttpStatus.NOT_MODIFIED)
    }

    private fun givenTheUserEndpoint(file: String, username: String) {
        val body = SynchronizationUseCaseTest::class.java.getResource("/${file}").readText()
        val headers = HttpHeaders().apply { put("date", listOf("Thu, 05 Jul 2012 15:31:30 GMT")) }
        val response: GithubClient.UserDetailsHttpResponse = jacksonObjectMapper().readValue(body)
        every {
            restTemplate.exchange<GithubClient.UserDetailsHttpResponse>(
                "https://api.github.com/users/${username}",
                HttpMethod.GET,
                any()
            )
        } returns ResponseEntity(response, headers, HttpStatus.OK)
    }

    private fun thenIExpectTheCodecentricDeveloperDocuments(vararg documents: Document) {
        val allMembers = mongo.findAll<Document>("codecentricDevs")
        assertThat(allMembers).containsExactlyElementsOf(documents.asList())
    }

    private fun thenIExpectANoContentResponse() {
        actions.andExpect(
            status().isNoContent
        )
    }

}
