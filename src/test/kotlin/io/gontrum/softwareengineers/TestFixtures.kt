package io.gontrum.softwareengineers

import net.minidev.json.JSONObject
import org.bson.Document
import org.json.JSONArray

fun aDeveloper(
    lastSyncRepos: String = "Thu, 05 Jul 2012 15:31:30 GMT",
    lastSyncUser: String = "Thu, 05 Jul 2012 15:31:30 GMT",
    repositoryUrl: String = "https://api.github.com/users/atestuser/repos",
    userUrl: String = "https://api.github.com/users/atestuser",
    id: String = "atestuser",
    name: String = "Miles Davis",
    languages: List<Document> = listOf(theLanguage("java", 1)),
    githubRepos: List<Document> = listOf(
        theGithubRepo(
            "my-great-project",
            "https://github.com/atestuser/my-great-project",
            "A Description"
        )
    )
): Document {
    return Document().apply {
        append("lastSyncRepos", lastSyncRepos)
        append("lastSyncUser", lastSyncUser)
        append("repositoryUrl", repositoryUrl)
        append("userUrl", userUrl)
        append("_id", id)
        append("name", name)
        append("languages", languages)
        append("githubRepos", githubRepos)
    }
}

fun theLanguage(name: String, times: Int): Document {
    return Document().apply {
        append("name", name)
        append("times", times)
    }
}

fun theGithubRepo(name: String, url: String, description: String): Document {
    return Document().apply {
        append("name", name)
        append("url", url)
        append("description", description)
    }
}

fun theLanguageResponse(name: String, times: Int): JSONObject {
    return JSONObject().apply {
        put("name", name)
        put("times", times)
    }
}

fun aDeveloperDeveloperDocument(
    lastSyncRepos: String = "Thu, 05 Jul 2012 15:31:30 GMT",
    lastSyncUser: String = "Thu, 05 Jul 2012 15:31:30 GMT",
    repositoryUrl: String = "https://api.github.com/users/atestuser/repos",
    userUrl: String = "https://api.github.com/users/atestuser",
    id: String = "atestuser",
    name: String = "Wynton Marsalis",
    languages: List<Document> = emptyList(),
    githubRepos: List<Document> = emptyList()
): Document {
    return Document().apply {
        append("lastSyncRepos", lastSyncRepos)
        append("lastSyncUser", lastSyncUser)
        append("repositoryUrl", repositoryUrl)
        append("userUrl", userUrl)
        append("_id", id)
        append("name", name)
        append("languages", languages)
        append("githubRepos", githubRepos)
    }
}

fun aDevResponse(name: String, username: String, vararg languages: JSONObject): JSONObject {
    return JSONObject().apply {
        put("username", username)
        put("name", name)
        put("languages", JSONArray(languages))
    }
}

fun aDevByLanguageResponse(login: String, name: String, ranking: Int): JSONObject {
    return JSONObject().apply {
        put("login", login)
        put("name", name)
        put("ranking", ranking)
    }
}
