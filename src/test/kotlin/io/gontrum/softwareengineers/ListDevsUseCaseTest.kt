package io.gontrum.softwareengineers

import io.gontrum.softwareengineers.controller.ListCodecentricDevController
import io.gontrum.softwareengineers.controller.SynchronizationController
import net.minidev.json.JSONObject
import org.bson.Document
import org.json.JSONArray
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@EnableMongoRepositories(basePackageClasses = [SoftwareengineersApplication::class])
@ContextConfiguration(
    initializers = [MongoContainerInitializer::class],
    classes = [ListCodecentricDevController::class, MongoConfiguration::class],
)
@AutoConfigureDataMongo
@WebMvcTest(SynchronizationController::class)
class ListDevsUseCaseTest(
    @Autowired val mongo: MongoTemplate,
    @Autowired val webClient: MockMvc
) {

    @AfterEach
    fun cleanup() {
        mongo.dropCollection("codecentricDevs")
    }

    @Test
    fun `should list all saved developers with all languages`() {
        givenTheCodecentricDevs(
            aDeveloperDeveloperDocument(
                name = "Wynton Marsalis",
                id = "marsalis",
                languages = listOf(
                    theLanguage("trumpet", 12),
                    theLanguage("scatting", 6),
                    theLanguage("saxophone", 2)
                )
            ),
            aDeveloperDeveloperDocument(
                name = "Louis Armstrong",
                id = "armstrong",
                languages = listOf(
                    theLanguage("trumpet", 15),
                    theLanguage("scatting", 1),
                )
            ),
            aDeveloperDeveloperDocument(
                name = "Bill Evans",
                id = "evans",
                languages = listOf(
                    theLanguage("piano", 15),
                )
            )
        )

        whenICallList()

        thenIExpectToReceive(
            aDevResponse(
                name = "Wynton Marsalis",
                username = "marsalis",
                theLanguageResponse("trumpet", 12),
                theLanguageResponse("scatting", 6),
                theLanguageResponse("saxophone", 2)
            ),
            aDevResponse(
                name = "Louis Armstrong",
                username = "armstrong",
                theLanguageResponse("trumpet", 15),
                theLanguageResponse("scatting", 1)
            ),
            aDevResponse(
                name = "Bill Evans",
                username = "evans",
                theLanguageResponse("piano", 15)
            )
        )
    }

    @Test
    fun `should filter by language and order by most projects with language`() {
        givenTheCodecentricDevs(
            aDeveloperDeveloperDocument(
                name = "Wynton Marsalis",
                id = "marsalis",
                languages = listOf(
                    theLanguage("trumpet", 12),
                    theLanguage("scatting", 6),
                    theLanguage("saxophone", 2)
                )
            ),
            aDeveloperDeveloperDocument(
                name = "Louis Armstrong",
                id = "armstrong",
                languages = listOf(
                    theLanguage("trumpet", 15),
                    theLanguage("scatting", 1),
                )
            ),
            aDeveloperDeveloperDocument(
                name = "Bill Evans",
                id = "evans",
                languages = listOf(
                    theLanguage("piano", 15),
                )
            )
        )

        whenICallListByLanguage("trumpet")

        thenIExpectToReceiveExactly(
            aDevByLanguageResponse(
                login = "armstrong",
                name = "Louis Armstrong",
                ranking = 15
            ),
            aDevByLanguageResponse(
                login = "marsalis",
                name = "Wynton Marsalis",
                ranking = 12
            ),
        )
    }

    private fun givenTheCodecentricDevs(vararg codecentricDevs: Document) {
        codecentricDevs.forEach {
            mongo.save(it, "codecentricDevs")
        }
    }

    private lateinit var actions: ResultActions

    private fun whenICallListByLanguage(language: String) {
        actions = webClient.perform(MockMvcRequestBuilders.get("/cc/devs/${language}"))
    }

    private fun whenICallList() {
        actions = webClient.perform(MockMvcRequestBuilders.get("/cc/devs"))
    }

    private fun thenIExpectToReceiveExactly(vararg jsons: JSONObject) {
        thenIExpectToReceive(*jsons, exactly = true)
    }

    private fun thenIExpectToReceive(vararg jsons: JSONObject, exactly: Boolean = false) {
        actions.andExpect(
            MockMvcResultMatchers.content().json(JSONArray(jsons).toString(), exactly)
        )
    }

}
