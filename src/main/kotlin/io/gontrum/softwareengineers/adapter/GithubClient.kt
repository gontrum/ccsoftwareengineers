package io.gontrum.softwareengineers.adapter

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import io.gontrum.softwareengineers.repository.GithubRepo
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.*
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

@Component
class GithubClient(
    @Qualifier("restTemplateGithub") private val restTemplate: RestTemplate,
    @Value("\${github.token}") private val token: String?,
    @Value("\${github.member.url}") private val memberUrl: String
) {

    private val MODIFIED_SINCE_HEADER = "If-Modified-Since"
    private val AUTHORIZATION_HEADER = "Authorization"

    fun getMembers(lastSync: String? = null): Pair<List<MemberHttpResponse>?, String?> {
        val response =
            restTemplate.exchange<List<MemberHttpResponse>>(
                memberUrl, HttpMethod.GET, requestHeaders(lastSync)
            )
        if (response.statusCode == HttpStatus.NOT_MODIFIED) return Pair(null, null)

        return Pair(response.body ?: emptyList(), response.timestamp())
    }

    fun callUserdetails(url: String, lastSync: String? = null): Pair<UserDetailsHttpResponse?, String?> {
        val response = restTemplate.exchange<UserDetailsHttpResponse>(
            url, HttpMethod.GET, requestHeaders(lastSync)
        )
        if (response.statusCode == HttpStatus.NOT_MODIFIED) return Pair(null, null)

        return Pair(response.body ?: throw IllegalStateException("FIXME"), response.timestamp())
    }

    fun getGithubRepos(url: String, lastSync: String? = null): Pair<List<RepositoryHttpResponse>?, String?> {
        val response = restTemplate.exchange<List<RepositoryHttpResponse>>(
            url, HttpMethod.GET, requestHeaders(lastSync)
        )
        if (response.statusCode == HttpStatus.NOT_MODIFIED) return Pair(null, null)

        return Pair(response.body ?: emptyList(), response.timestamp())
    }

    private fun requestHeaders(lastSync: String?) = HttpEntity<Any>(
        HttpHeaders().apply {
            lastSync?.let { put(MODIFIED_SINCE_HEADER, listOf(lastSync)) }
            if (!token.isNullOrEmpty()) {
                put(AUTHORIZATION_HEADER, listOf("bearer ${token}"))
            }
        }
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class MemberHttpResponse(
        val login: String,
        @JsonProperty("url") val userDetailsUrl: String,
        @JsonProperty("html_url") val htmlUrl: String,
        @JsonProperty("repos_url") val repositoriesUrl: String
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class UserDetailsHttpResponse(
        val name: String?,
        val url: String
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class RepositoryHttpResponse(
        val language: String?,
        @JsonProperty("html_url") val projectUrl: String,
        val name: String,
        val description: String?
    ) {
        fun toGithubRepo() =
            GithubRepo(
                name,
                projectUrl,
                description
            )

    }
}

fun <T> ResponseEntity<T>.timestamp() = headers["date"]?.first()
