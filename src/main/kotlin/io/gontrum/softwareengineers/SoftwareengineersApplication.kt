package io.gontrum.softwareengineers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.MongoDatabaseFactory
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.data.mongodb.core.mapping.MongoMappingContext


@SpringBootApplication
class SoftwareengineersApplication

fun main(args: Array<String>) {
    runApplication<SoftwareengineersApplication>(*args)
}

@Configuration
class RestTemplateConfiguration {
    @Bean("restTemplateGithub")
    fun restTemplate() = RestTemplateBuilder().build()
}


@Configuration
class MongoConfiguration(
    @Autowired private val mongoDbFactory: MongoDatabaseFactory,
    @Autowired private val mongoMappingContext: MongoMappingContext
) {
    @Bean
    fun mappingMongoConverter(): MappingMongoConverter {
        val dbRefResolver = DefaultDbRefResolver(mongoDbFactory)
        val converter = MappingMongoConverter(dbRefResolver, mongoMappingContext)
        converter.setTypeMapper(DefaultMongoTypeMapper(null))
        return converter
    }
}

