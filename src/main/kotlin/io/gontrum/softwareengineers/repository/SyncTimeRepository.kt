package io.gontrum.softwareengineers.repository

import io.gontrum.softwareengineers.repository.SyncTimes
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface SyncTimeRepository : MongoRepository<SyncTimes, String>
