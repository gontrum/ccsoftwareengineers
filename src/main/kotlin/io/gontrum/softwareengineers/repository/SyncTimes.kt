package io.gontrum.softwareengineers.repository

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "syncTimes")
data class SyncTimes(
    @Id val name: String,
    val timestamp: String
)
