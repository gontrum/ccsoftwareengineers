package io.gontrum.softwareengineers.repository

import io.gontrum.softwareengineers.controller.ListCodecentricDevController
import org.springframework.data.mongodb.repository.Aggregation
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface CodecentricDevRepository : MongoRepository<CodecentricDeveloper, String> {

    @Aggregation(
        pipeline = [
            "{\$unwind: \"\$languages\"}",
            "{\$match: {\"languages.name\": \"?0\"}}",
            "{\$sort: {\"languages.times\": -1}}",
            "{\$addFields: { \"ranking\": \"\$languages.times\"}}",
            "{\$addFields: { \"login\": \"\$_id\"}}",
            "{\$project: { \"login\": 1, name: 1, ranking: 1}}"
        ]
    )
    fun orderByLanguage(language: String): List<ListCodecentricDevController.ListDevByLanguageResponse>
}
