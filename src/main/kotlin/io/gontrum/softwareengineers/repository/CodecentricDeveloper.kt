package io.gontrum.softwareengineers.repository

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "codecentricDevs")
data class CodecentricDeveloper(
    @Id val id: String,
    val lastSyncRepos: String?,
    val lastSyncUser: String?,
    val name: String,
    val languages: List<Language>,
    val githubRepos: List<GithubRepo>,
    val repositoryUrl: String,
    val userUrl: String
)

data class Language(
    @Indexed
    val name: String,
    val times: Int
)

data class GithubRepo(
    val name: String,
    val url: String,
    val description: String?
)
