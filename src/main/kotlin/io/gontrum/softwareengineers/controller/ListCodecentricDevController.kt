package io.gontrum.softwareengineers.controller

import io.gontrum.softwareengineers.repository.CodecentricDevRepository
import io.gontrum.softwareengineers.repository.Language
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class ListCodecentricDevController(@Autowired private val codecentricDevRepository: CodecentricDevRepository) {

    @GetMapping("/cc/devs")
    fun listDevs(): List<ListDevResponse> {
        val allDevs = codecentricDevRepository.findAll()
        return allDevs.map {
            ListDevResponse(
                name = it.name,
                username = it.id,
                languages = it.languages
            )
        }
    }

    @GetMapping("/cc/devs/{language}")
    fun listDevByLanguage(@PathVariable language: String): List<ListDevByLanguageResponse> {
        return codecentricDevRepository.orderByLanguage(language)
    }

    data class ListDevResponse(
        val name: String?,
        val username: String,
        val languages: List<Language>
    )

    data class ListDevByLanguageResponse(
        val login: String,
        val name: String,
        val ranking: Int
    )
}
