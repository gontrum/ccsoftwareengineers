package io.gontrum.softwareengineers.controller

import io.gontrum.softwareengineers.service.SynchronizationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class SynchronizationController(
    @Autowired private val synchronizationService: SynchronizationService
) {

    @PutMapping("/synchronize")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun synchronize() {
        synchronizationService.startSync()
    }
}
