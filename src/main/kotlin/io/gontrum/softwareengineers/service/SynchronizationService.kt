package io.gontrum.softwareengineers.service

import io.gontrum.softwareengineers.adapter.GithubClient
import io.gontrum.softwareengineers.repository.CodecentricDevRepository
import io.gontrum.softwareengineers.repository.SyncTimeRepository
import io.gontrum.softwareengineers.repository.SyncTimes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class SynchronizationService(
    @Autowired private val client: GithubClient,
    @Autowired private val syncTimeRepository: SyncTimeRepository,
    @Autowired private val codecentricDevRepository: CodecentricDevRepository
) {

    fun startSync() {
        val lastMemberCall = findLastMemberCall()

        val (membersFromEndpoint, updatedMemberTimestamp) = client.getMembers(lastSync = lastMemberCall)
        updatedMemberTimestamp?.let {
            syncTimeRepository.save(SyncTimes(name = "members", timestamp = it))
        }

        removeDevsWhichDontExistAnymore(membersFromEndpoint)

        val devs = membersFromEndpoint
            ?.map(this::createFromExistingDeveloperOrElseFromMemberEndpointObject)
            ?: codecentricDevRepository.findAll().map(CodecentricDevDataPreperator::fromDatabaseDocument)

        val dbDocumentsToUpdate = devs.map { dev ->
            val (userDetails, userSyncedAt) = client.callUserdetails(dev.userDetailsUrl, dev.lastUserSync)
            val (repositories, reposSyncedAt) = client.getGithubRepos(dev.repositoriesUrl, dev.lastRepoSync)
            dev.updateUserDetails(userDetails)
                .updateRepositories(repositories)
                .updateUserSyncTimestamp(userSyncedAt)
                .updateRepoSyncTimestamp(reposSyncedAt)
                .toDatabaseDocument()
        }

        dbDocumentsToUpdate.forEach(codecentricDevRepository::save)
    }

    private fun removeDevsWhichDontExistAnymore(membersFromEndpoint: List<GithubClient.MemberHttpResponse>?) {
        membersFromEndpoint?.map { it.login }?.let { memberLogins ->
            val noLongerExistingDevs = codecentricDevRepository.findAll()
                .map { it.id }
                .filterNot { existingDevId -> memberLogins.contains(existingDevId) }
            noLongerExistingDevs.forEach { devToDelete -> codecentricDevRepository.deleteById(devToDelete) }
        }
    }

    private fun findLastMemberCall() = syncTimeRepository.findByIdOrNull("members")?.timestamp

    private fun createFromExistingDeveloperOrElseFromMemberEndpointObject(member: GithubClient.MemberHttpResponse) =
        codecentricDevRepository.findByIdOrNull(member.login)?.let(CodecentricDevDataPreperator::fromDatabaseDocument)
            ?: CodecentricDevDataPreperator.fromMemberResponse(member)
}

