package io.gontrum.softwareengineers.service

import io.gontrum.softwareengineers.adapter.GithubClient
import io.gontrum.softwareengineers.repository.CodecentricDeveloper
import io.gontrum.softwareengineers.repository.GithubRepo
import io.gontrum.softwareengineers.repository.Language
import java.util.*

data class CodecentricDevDataPreperator(
    val login: String,
    val userDetailsUrl: String,
    val repositoriesUrl: String,
    val lastUserSync: String? = null,
    val lastRepoSync: String? = null,
    val name: String? = null,
    val languages: List<Language>? = null,
    val githubRepos: List<GithubRepo>? = null,
    val htmlUrl: String? = null
) {

    fun toDatabaseDocument(): CodecentricDeveloper {
        return CodecentricDeveloper(
            id = login,
            lastSyncRepos = lastRepoSync,
            lastSyncUser = lastUserSync,
            name = name ?: login,
            languages = languages ?: emptyList(),
            githubRepos = githubRepos ?: emptyList(),
            repositoryUrl = repositoriesUrl,
            userUrl = userDetailsUrl
        )
    }

    fun updateUserDetails(userDetails: GithubClient.UserDetailsHttpResponse?): CodecentricDevDataPreperator {
        return copy(
            name = userDetails?.name ?: name
        )
    }

    fun updateRepositories(repositories: List<GithubClient.RepositoryHttpResponse>?): CodecentricDevDataPreperator {
        return copy(
            languages = repositories?.let { extractLanguages(it) } ?: languages,
            githubRepos = repositories?.map { repo -> repo.toGithubRepo() } ?: githubRepos
        )
    }

    fun updateUserSyncTimestamp(userSyncedAt: String?): CodecentricDevDataPreperator {
        return copy(lastUserSync = userSyncedAt ?: lastUserSync)
    }

    fun updateRepoSyncTimestamp(reposSyncedAt: String?): CodecentricDevDataPreperator {
        return copy(lastRepoSync = reposSyncedAt ?: lastRepoSync)
    }

    private fun extractLanguages(githubRepoResults: List<GithubClient.RepositoryHttpResponse>) =
        githubRepoResults.filter { it.language != null }.map { it.language!!.lowercase(Locale.US) }.groupingBy { it }
            .eachCount()
            .map { (name, times) -> Language(name, times) }

    companion object {
        fun fromDatabaseDocument(ccDev: CodecentricDeveloper): CodecentricDevDataPreperator {
            return CodecentricDevDataPreperator(
                login = ccDev.id,
                name = ccDev.name,
                userDetailsUrl = ccDev.userUrl,
                repositoriesUrl = ccDev.repositoryUrl,
                lastUserSync = ccDev.lastSyncUser,
                lastRepoSync = ccDev.lastSyncRepos,
                languages = ccDev.languages,
                githubRepos = ccDev.githubRepos
            )
        }

        fun fromMemberResponse(member: GithubClient.MemberHttpResponse): CodecentricDevDataPreperator {
            return CodecentricDevDataPreperator(
                login = member.login,
                userDetailsUrl = member.userDetailsUrl,
                repositoriesUrl = member.repositoriesUrl,
                htmlUrl = member.htmlUrl
            )
        }
    }
}
