# Github Ranking

Ein kleines Projekt, mit dessen Hilfe abgerufen werden kann wie viele Projekte Devs mit welcher Sprache bereits
umgesetzt haben.

## Einstellungen

Folge Eigenschaften sind konfigurierbar:

- `spring.data.mongodb.uri`: Die URI zur Datenbank. Hierüber erfolgt auch die Autorisierung
- `github.token`: Das Token, über das mit Github kommuniziert werden soll (siehe
  ggf: [Token generieren](https://github.com/settings/tokens)). Ein Betrieb ohne Token ist möglich aber stark
  eingeschränkt, da nur public Repos angezeigt werden und nur wenige Abfragen pro IP-Adresse möglich sind.
- `github.member.url`: Die Einstiegs URL zur github-Organisation. Diese kann bei Bedarf umkonfiguriert werden

Um die Eigenschaften zu konfigurieren, gibt es unterschiedliche Möglichkeiten, die auf folgender Website erläutert
werden: [Spring Docs](https://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/html/boot-features-external-config.html#boot-features-external-config-command-line-args)

## Endpunkte

`/synchronize`: Startet die Synchronisation mit Github und läd alle benötigten Informationen herunter
`/cc/devs`: Listet alle synchronisierten Devs in einer kurzen Übersicht auf
`/cc/dev/$programmiersprache`: Filtert die Devs, die Projekte mit der angegebenen Programmiersprache haben und ordnet
sie nach deren Anzahl

## Lokal starten

Um die Anwendung lokal zu testen, bietet es sich an eine MongoDB-Instanz über Docker hochzufahren:
```shell
docker run -d -p27017:27017 \
    -e MONGO_INITDB_ROOT_USERNAME=mongo \
    -e MONGO_INITDB_ROOT_PASSWORD=mongo \
    mongo:4.2
```
Beim Start der Anwendung muss dann den Parameter
`spring.data.mongodb.uri=mongodb://mongo:mongo@localhost:27018/test`
angeben. Auch der Parameter `github.token` sollte gesetzt werden, da die Anwendung sonst sehr stark eingeschränkt ist. 

Initial müssen die Daten synchronisiert werden, indem der synchronize Endpoint aufgerufen wird. Ein Beispiel dafür findet sich in der Datei `endpoints.http`

## Offene Themen

- Eine Webanwendung über die Endanwender die Benutzer filtern können
- Grundsätzliche Fehlerbehandlung
- CI/CD
